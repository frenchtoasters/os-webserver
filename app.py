from flask import Flask, make_response, send_file

# set the project root directory as the static folder, you can set others.
app = Flask(__name__, static_url_path='')

@app.route('/bootstrap')
def boostrap():
    resp = make_response(send_file('/bootstrap.ign'), 200)
    resp.mimetype = "application/vnd.coreos.ignition+json"
    return resp

if __name__ == "__main__":
    app.run(host='0.0.0.0', port='5000', debug=False)