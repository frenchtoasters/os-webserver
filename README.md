# OpenShift 4.1 Webserver

With OpenShift 4.1 release comes a new method for deploying infrastructure hosts, this library contains a basic `Flask` web app that will serve the `bootstrap.ign` file that is needed to bootstrap the OpenShift 4.1 cluster. 